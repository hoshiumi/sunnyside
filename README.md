# Sunnyside Agency Template using HTML,CSS,JS

This is a solution to the [Sunnyside agency landing page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/sunnyside-agency-landing-page-7yVs3B6ef)

## Table of contents

-   [Overview](#overview)
    -   [The challenge](#the-challenge)
    -   [Screenshot](#screenshot)
    -   [Links](#links)
-   [My process](#my-process)
    -   [Built with](#built-with)
    -   [What I learned](#what-i-learned)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

-   View the optimal layout for the site depending on their device's screen size
-   See hover states for all interactive elements on the page

### Screenshot

![Image of the challenge](https://res.cloudinary.com/dz209s6jk/image/upload/q_auto,w_700/Challenges/pigotdvhcch2b3wp9uuc.jpg)

### Links

-   Solution URL: [Add solution URL here](https://www.frontendmentor.io/solutions/sunnyside-agency-template-using-htmlcssjs-nw_eiFHwz)
-   Live Site URL: [Add live site URL here](https://cocky-bartik-a4bdcf.netlify.app/)

## My process

### Built with

-   Semantic HTML5 markup
-   SCSS
-   Flexbox
-   CSS Grid
-   Desktop-first workflow
-   [Vitejs](https://vitejs.dev/)

### What I learned

This challenge was the first I tried, but I gave up because I was not able to do the media queries. So this solution is my revenge :). This challenge just improved my skills in HTML and CSS.
